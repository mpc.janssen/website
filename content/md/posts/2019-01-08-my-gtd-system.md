{:title "My GTD system"
 :layout :post
 :tags  ["gtd" ]
 :toc false
}

## Note to self

NOTE: Focus on the process not on the tools!!

## Process

### Ubiquitous capture

### Open formats

I do not want my stuff ending up in some closed database or format which
is not easily transferable to something else. This means that for
instance Evernote is not the best match. Instead I use plain text files
as much as possible.

* Todo file: todo.txt plain text format
* Minutes and documents: org-mode syntax

For the actual next action list, the plain text open format requirement
is less stringent. Tasks are generally easy to export and less
longlived. In this case having a good application/tool to do task
management is more important.

### Tools

Use tools that are intended for the purpose. For instance using Evernote
as a task list is a poor fit and doesn't work very well.

* Storage: Dropbox or any other file/directory sync
* Task list (Android): Simpletask
* Task list (Windows): todotxt.net
